# Nutrical

Nutrical is an android application about food and especially nutrition. The application targets Android API 28 Oreo. Nutrical recipe’s collection is based on the API Spoonacular which offers a complex food ontology.
This application was developped during a school project (Android M1 - Project).

# Functionnalities

- Get food trivia
- See favorite recipe
- Set diet and food intolerances
- Enable notifications
- Enable dark mode
- Read recipe’s instructions
- Find recipe’s nutrition
- Add a recipe to favorites
- Read FAQ and See About
