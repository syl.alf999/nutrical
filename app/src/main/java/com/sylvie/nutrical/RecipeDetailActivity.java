package com.sylvie.nutrical;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.sylvie.nutrical.model.Recipe;
import com.sylvie.nutrical.utils.Constant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class RecipeDetailActivity extends OptionsMenuActivity {
    FloatingActionButton fabAddToFavorite;
    ImageView ivRecipePictureDetail;
    TextView tvRecipeNameDetail;
    Toolbar toolbar;
    TabLayout tabLayout;
    TabItem tabRecipe, tabNutrition;
    ViewPager viewPager;
    Recipe recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        //Setting the toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivRecipePictureDetail = findViewById(R.id.ivRecipePictureDetail);
        tvRecipeNameDetail = findViewById(R.id.tvRecipeNameDetail);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String imageUrl = intent.getStringExtra("imageUrl");
        String instructions = intent.getStringExtra("instructions");
        HashMap<String, String> nutritionsValues = (HashMap<String, String>) intent.getSerializableExtra("nutritionsValues");
        HashMap<String, String> nutritionsPercentOfDailyNeeds = (HashMap<String, String>) intent.getSerializableExtra("nutritionsPercentOfDailyNeeds");

        this.recipe = new Recipe(title, imageUrl, instructions, nutritionsValues, nutritionsPercentOfDailyNeeds);
        //this.recipe =  intent.getParcelableExtra("recipe");

        tvRecipeNameDetail.setText(recipe.getName());

        fabAddToFavorite = findViewById(R.id.fabAddToFavorite);
        setIconOfFabAddToFavorite();

        tabLayout = findViewById(R.id.tabLayout);
        tabNutrition = findViewById(R.id.tabNutrition);
        tabRecipe = findViewById(R.id.tabRecipe);

        viewPager = findViewById(R.id.view_pager);

        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        onTabSelectedClicked();

        Glide
            .with(this)
            .load(recipe.getImageUrl())
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .into(ivRecipePictureDetail);
    }

    /**
     * Implement the actions when the user select a tab
     * Recipe | Nutrition
     */
    public void onTabSelectedClicked(){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * Set the right icon wether the current recipe is in the favorite or not
     */
    public void setIconOfFabAddToFavorite() {
        SharedPreferences sp = getSharedPreferences(Constant.PREFS_FILE_NAME, MODE_PRIVATE);
        Set<String> favoriteRecipes = sp.getStringSet("favoriteRecipes", new HashSet<String>());

        if(favoriteRecipes.contains(recipe.getName())){
            fabAddToFavorite.setImageResource(R.drawable.ic_favorite);
        }else{
            fabAddToFavorite.setImageResource(R.drawable.ic_favorite_border);
        }
    }

    /**
     * Add and remove recipe from favorites
     * Change icon of the floating action buttons when clicked
     */
    public void onClickFABFavorite(View view){

        // BE CAREFUL, when modifying strings sets in shared preferences
        // Should not modify directly the Set<String> returned by sp.getStringSet()
        //src : https://stackoverflow.com/questions/10720028/android-sharedpreferences-not-saving
        SharedPreferences sp = getSharedPreferences(Constant.PREFS_FILE_NAME, MODE_PRIVATE);
        Set<String> favoriteRecipes = new HashSet<>(sp.getStringSet(Constant.PREFS_favoriteRecipes, new HashSet<>()));

        //REMOVING
        if(favoriteRecipes.contains(recipe.getName())){
            favoriteRecipes.remove(recipe.getName());
            fabAddToFavorite.setImageResource(R.drawable.ic_favorite_border);
        }
        else{ //ADDING
            favoriteRecipes.add(recipe.getName());
            fabAddToFavorite.setImageResource(R.drawable.ic_favorite);
        }

        SharedPreferences.Editor editor = sp.edit();
        editor.putStringSet(Constant.PREFS_favoriteRecipes, new HashSet<>(favoriteRecipes));
        editor.commit();
    }


    private class PagerAdapter extends FragmentPagerAdapter {
        private int numberOfTabs;

        public PagerAdapter(FragmentManager fm, int numberOfTabs) {
            super(fm);
            this.numberOfTabs = numberOfTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new RecipeInstructionFragment(recipe.getInstructions());
                case 1:
                    return new NutritionFragment(recipe);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return this.numberOfTabs;
        }

    }
}