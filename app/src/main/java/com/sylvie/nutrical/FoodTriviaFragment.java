package com.sylvie.nutrical;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sylvie.nutrical.utils.Constant;
import com.sylvie.nutrical.utils.VolleySingleton;

import org.json.JSONException;

public class FoodTriviaFragment extends Fragment {
    TextView tvFoodTrivia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_food_trivia, container, false);
        tvFoodTrivia = view.findViewById(R.id.tvFoodTrivia);

        SharedPreferences sp = this.getActivity().getSharedPreferences(Constant.PREFS_FILE_NAME, this.getActivity().MODE_PRIVATE);
        String message = sp.getString(Constant.PREFS_foodTrivia,  Constant.SOMETHING_WENT_WRONG_WITH_THE_SERVER);
        tvFoodTrivia.setText(message);

        return view;
    }
}