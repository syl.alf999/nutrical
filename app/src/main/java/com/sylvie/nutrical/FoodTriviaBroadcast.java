package com.sylvie.nutrical;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sylvie.nutrical.utils.App;
import com.sylvie.nutrical.utils.Constant;
import com.sylvie.nutrical.utils.VolleySingleton;
import org.json.JSONException;


public class FoodTriviaBroadcast extends BroadcastReceiver {
    private NotificationManagerCompat notificationManager;
    
    @Override
    public void onReceive(Context context, Intent intent) {
        notificationManager = NotificationManagerCompat.from(context);
        getFoodTrivia(context);
        sendOnChannel(context);
    }

    /**
     * Make API call to get a random food trivia
     * and save the trivia in the SharedPreferences
     * @param context
     */
    public void getFoodTrivia(Context context){
        RequestQueue requestQueue = VolleySingleton.getInstance(context).getRequestQueue();

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, Constant.URL_RANDOM_TRIVIA, null,
                response -> {
                    try {
                        SharedPreferences sp = context.getSharedPreferences(Constant.PREFS_FILE_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();

                        String text = response.getString("text");
                        editor.putString(Constant.PREFS_foodTrivia, text);
                        editor.apply();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                Throwable::printStackTrace
        );

        objectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(context).addToRequestQueue(objectRequest);
    }

    /**
     * Build and send notification in an existing channel
     * The notification text is initialized with the saved food trivia from the
     * SharedPreferences
     * @param context
     */
    private void sendOnChannel(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constant.PREFS_FILE_NAME, Context.MODE_PRIVATE);
        String message = sp.getString(Constant.PREFS_foodTrivia, Constant.SOMETHING_WENT_WRONG_WITH_THE_SERVER);


        Notification notification = new NotificationCompat.Builder(context, App.NOTIFICATION_CHANEL_FOOD_TRIVIA)
                .setSmallIcon(R.drawable.ic_serving_dish)
                .setContentTitle("Food Trivia")
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_RECOMMENDATION)
                .build();

        notificationManager.notify(1, notification);
    }
}