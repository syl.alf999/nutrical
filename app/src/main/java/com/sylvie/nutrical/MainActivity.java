package com.sylvie.nutrical;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sylvie.nutrical.model.Recipe;
import com.sylvie.nutrical.utils.Constant;
import com.sylvie.nutrical.utils.VolleySingleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends OptionsMenuActivity  implements RecipeAdapter.OnItemClickListener{
    private RecyclerView favoriteRecipesRecyclerView;
    private RecipeAdapter recipeAdapter;
    private List<Recipe> recipeList;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setting the toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        favoriteRecipesRecyclerView = findViewById(R.id.favoriteRecipesRecyclerView);
        favoriteRecipesRecyclerView.setHasFixedSize(true); //because the width is fixed
        favoriteRecipesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        recipeList = new ArrayList<>();

        getFavouriteRecipes();
    }


    private void getFavouriteRecipes() {
        SharedPreferences sp = getSharedPreferences(Constant.PREFS_FILE_NAME, MODE_PRIVATE);
        Set<String> favoriteRecipes = sp.getStringSet(Constant.PREFS_favoriteRecipes, new HashSet<String>());

        for (String dishName : favoriteRecipes) {

            String url = Constant.GET_URL_SEARCH_RECIPE(dishName);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    url, null,
                    response -> {
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                fetchingRecipe(jsonArray.getJSONObject(i));
                            }
                            recipeAdapter = new RecipeAdapter(MainActivity.this, recipeList);
                            favoriteRecipesRecyclerView.setAdapter(recipeAdapter);
                            recipeAdapter.setOnItemClickListener(MainActivity.this);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }, error -> error.printStackTrace());

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        }
    }

    /**
     * Fetch recipe information from a JSONObject
     * @param recipe
     * @throws JSONException
     */
    public void fetchingRecipe(JSONObject recipe) throws JSONException {
        String recipeName = recipe.getString("title");
        String imageURL = recipe.getString("image");
        JSONArray nutrients =  recipe.getJSONObject("nutrition").getJSONArray("nutrients");

        HashMap<String, String> nutritionsValues = fetchNutritionsValues(nutrients);
        HashMap<String, String> nutritionsPercentOfDailyNeeds = fetchNutritionsPercentOfDailyNeeds(nutrients);

        JSONArray instructionSteps = recipe.getJSONArray("analyzedInstructions").getJSONObject(0).getJSONArray("steps");
        StringBuilder sb = new StringBuilder();

        for (int j = 0; j < instructionSteps.length() ; j++) {
            sb.append(instructionSteps.getJSONObject(j).getString("number") + ". " + instructionSteps.getJSONObject(j).getString("step"));
            sb.append(System.lineSeparator());
        }

        Recipe recipeObj = new Recipe(recipeName, imageURL, sb.toString(),nutritionsValues, nutritionsPercentOfDailyNeeds);
        recipeList.add(recipeObj);
    }

    /**
     * Fetch nutritions values from a JSONArray
     * @param nutrients
     * @return
     * @throws JSONException
     */
    public HashMap<String, String> fetchNutritionsValues(JSONArray nutrients) throws JSONException {
        HashMap<String, String> nutritionsValues = new HashMap<>();

        String calories = nutrients.getJSONObject(0).getString("amount") + nutrients.getJSONObject(0).getString("unit");
        String fat = nutrients.getJSONObject(1).getString("amount") + nutrients.getJSONObject(1).getString("unit");
        String carbs = nutrients.getJSONObject(3).getString("amount") + nutrients.getJSONObject(3).getString("unit");
        String protein = nutrients.getJSONObject(8).getString("amount") + nutrients.getJSONObject(8).getString("unit");

        nutritionsValues.put(Recipe.KEY_CALORIES, calories);
        nutritionsValues.put(Recipe.KEY_FAT, fat);
        nutritionsValues.put(Recipe.KEY_CARBS, carbs);
        nutritionsValues.put(Recipe.KEY_PROTEIN, protein);

        return nutritionsValues;
    }

    /**
     * Fetch each nutrition percent of daily needs
     * @param nutrients
     * @return
     * @throws JSONException
     */
    public HashMap<String, String> fetchNutritionsPercentOfDailyNeeds(JSONArray nutrients) throws JSONException {
        HashMap<String, String> nutritionsPercentOfDailyNeeds = new HashMap<>();

        String caloriesPercentOfDailyNeeds = nutrients.getJSONObject(0).getString("percentOfDailyNeeds");
        String fatPercentOfDailyNeeds = nutrients.getJSONObject(1).getString("percentOfDailyNeeds");
        String carbsPercentOfDailyNeeds = nutrients.getJSONObject(3).getString("percentOfDailyNeeds");
        String proteinPercentOfDailyNeeds = nutrients.getJSONObject(8).getString("percentOfDailyNeeds");

        nutritionsPercentOfDailyNeeds.put(Recipe.KEY_CALORIES, caloriesPercentOfDailyNeeds);
        nutritionsPercentOfDailyNeeds.put(Recipe.KEY_FAT, fatPercentOfDailyNeeds);
        nutritionsPercentOfDailyNeeds.put(Recipe.KEY_CARBS, carbsPercentOfDailyNeeds);
        nutritionsPercentOfDailyNeeds.put(Recipe.KEY_PROTEIN, proteinPercentOfDailyNeeds);

        return nutritionsPercentOfDailyNeeds;
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(MainActivity.this, RecipeDetailActivity.class);
        Recipe clickedRecipe = recipeList.get(position);

        intent.putExtra("title", clickedRecipe.getName());
        intent.putExtra("imageUrl", clickedRecipe.getImageUrl());
        intent.putExtra("instructions", clickedRecipe.getInstructions());
        intent.putExtra("nutritionsValues", clickedRecipe.getNutritionsValues());
        intent.putExtra("nutritionsPercentOfDailyNeeds", clickedRecipe.getNutritionsPercentOfDailyNeeds());
        startActivity(intent);
    }
}