package com.sylvie.nutrical;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class RecipeInstructionFragment extends Fragment {
    RecipeDetailActivity activity;
    TextView tvRecipeInstructions;
    String instructions;

    public RecipeInstructionFragment(String instructions) {
        this.instructions = instructions;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe_instruction, container, false);
        tvRecipeInstructions = view.findViewById(R.id.tvRecipeInstructions);

        tvRecipeInstructions.setText(instructions);
        tvRecipeInstructions.setMovementMethod(new ScrollingMovementMethod());
        return view;
    }
}