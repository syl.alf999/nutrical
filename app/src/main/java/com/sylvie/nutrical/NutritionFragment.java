package com.sylvie.nutrical;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sylvie.nutrical.model.Recipe;

public class NutritionFragment extends Fragment {
    TextView tvCaloriesValue, tvCarbsValue, tvFatValue, tvProteinValue;
    ProgressBar progressBarCalories, progressBarCarbs, progressBarFat, progressBarProtein;
    TextView tvCaloriesPercentage, tvCarbsPercentage, tvFatPercentage, tvProteinPercentage;
    private Recipe recipe;

    public NutritionFragment(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nutrition, container, false);

        tvCaloriesValue = view.findViewById(R.id.tvCaloriesValue);
        tvCarbsValue = view.findViewById(R.id.tvCarbsValue);
        tvFatValue = view.findViewById(R.id.tvFatValue);
        tvProteinValue = view.findViewById(R.id.tvProteinValue);

        progressBarCalories = view.findViewById(R.id.progressBarCalories);
        progressBarCarbs = view.findViewById(R.id.progressBarCarbs);
        progressBarFat = view.findViewById(R.id.progressBarFat);
        progressBarProtein = view.findViewById(R.id.progressBarProtein);

        tvCaloriesPercentage = view.findViewById(R.id.tvCaloriesPercentage);
        tvCarbsPercentage = view.findViewById(R.id.tvCarbsPercentage);
        tvFatPercentage= view.findViewById(R.id.tvFatPercentage);
        tvProteinPercentage = view.findViewById(R.id.tvProteinPercentage);

        tvCaloriesValue.setText(recipe.getNutritionsValues().get(Recipe.KEY_CALORIES));
        tvCarbsValue.setText(recipe.getNutritionsValues().get(Recipe.KEY_CARBS));
        tvFatValue.setText(recipe.getNutritionsValues().get(Recipe.KEY_FAT));
        tvProteinValue.setText(recipe.getNutritionsValues().get(Recipe.KEY_PROTEIN));

        int caloriesPercent = (int) Double.parseDouble(recipe.getNutritionsPercentOfDailyNeeds().get(Recipe.KEY_CALORIES));
        int carbsPercent = (int) Double.parseDouble(recipe.getNutritionsPercentOfDailyNeeds().get(Recipe.KEY_CARBS));
        int fatPercent = (int) Double.parseDouble(recipe.getNutritionsPercentOfDailyNeeds().get(Recipe.KEY_FAT));
        int proteinPercent = (int) Double.parseDouble(recipe.getNutritionsPercentOfDailyNeeds().get(Recipe.KEY_PROTEIN));

        progressBarCalories.setProgress(caloriesPercent);
        tvCaloriesPercentage.setText(caloriesPercent+"%");

        progressBarProtein.setProgress(proteinPercent);
        tvProteinPercentage.setText(proteinPercent+"%");

        progressBarFat.setProgress(fatPercent);
        tvFatPercentage.setText(fatPercent+"%");

        progressBarCarbs.setProgress(carbsPercent);
        tvCarbsPercentage.setText(carbsPercent+"%");

        return view;
    }
}