package com.sylvie.nutrical;

import android.os.Bundle;

/**
 * FAQ Activity
 */
public class FaqActivity extends OptionsMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
    }
}