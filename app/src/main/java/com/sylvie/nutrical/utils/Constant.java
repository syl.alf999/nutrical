package com.sylvie.nutrical.utils;

import android.content.SharedPreferences;
import android.util.Log;

import com.sylvie.nutrical.BuildConfig;
import com.sylvie.nutrical.model.Diet;

import java.util.HashSet;
import java.util.Set;

/**
 * Define the constants used throughout the application
 */
public class Constant {

    //PREFERENCES
    public static final String PREFS_FILE_NAME = "com.sylvie.nutrical.Settings";
    public static final String PREFS_darkModeEnabled = "darkModeEnabled";
    public static final String PREFS_notificationsEnabled = "notificationsEnabled";
    public static final String PREFS_foodTrivia = "foodTrivia";
    public static final String PREFS_diet = "diet";
    public static final String PREFS_intolerances = "intolerance";
    public static final String PREFS_favoriteRecipes = "favoriteRecipes";

    //RESTRICTING NUMBER OF RESULTS
    private static final int NUMBER_OF_RESULTS_IN_FOUNDED_RECIPES = 5;

    //BASE URL
    public static final String URL_RANDOM_TRIVIA = "https://api.spoonacular.com/food/trivia/random?apiKey="+ BuildConfig.API_KEY;
    private static final String URL_SEARCH_RECIPE = "https://api.spoonacular.com/recipes/complexSearch?number="+ NUMBER_OF_RESULTS_IN_FOUNDED_RECIPES +"&addRecipeNutrition=true&apiKey=" + BuildConfig.API_KEY;
    private static final String URL_SEARCH_RECIPE_BY_TITLE_MATCH = "https://api.spoonacular.com/recipes/complexSearch?number=1&addRecipeNutrition=true&apiKey=" + BuildConfig.API_KEY;
    public static final String URL_GET_RANDOM_RECIPE = "https://api.spoonacular.com/recipes/random?number=1&apiKey=" + BuildConfig.API_KEY;

    //ERRORS MESSAGE
    public static final String SOMETHING_WENT_WRONG_WITH_THE_SERVER = "Something went wrong with the server...";

    /**
     * Used to get many results from a dish title
     * @param dishTitle
     * @return
     */
    public static String GET_URL_SEARCH_RECIPE(String dishTitle){
        return URL_SEARCH_RECIPE + "&query=" + dishTitle;
    }

    /**
     * Used to get one result matching exatly the title of the dish
     * Each recipe in Spoonacular has unique name
     * @param dishTitle
     * @return
     */
    public static String GET_URL_SEARCH_RECIPE_BY_TITLE_MATCH(String dishTitle){
        return URL_SEARCH_RECIPE_BY_TITLE_MATCH + "&titleMatch=" + dishTitle;
    }

    /**
     * Format correctly the query paramaters of intolerance for the API call
     * The intolerances are separated by commas
     * If the user didn't specify any intolerances we return an empty string
     * @return string
     */
    public static String FORMAT_DIET_AND_INTOLERANCES_PARAMETERS(SharedPreferences sp){
        Set<String> intolerances = sp.getStringSet(Constant.PREFS_intolerances, new HashSet<String>());
        String diet = sp.getString(PREFS_diet, "no_particular_diet");

        //Return empty string
        if(intolerances.isEmpty() && diet.equals("no_particular_diet"))
            return "";


        StringBuilder sb = new StringBuilder();

        //Add diet parameter
        if(!diet.equals(Diet.NoDiet.toString())){
            sb.append("&diet=");
            if(diet.equals(Diet.Pescetarian.toString()))
                sb.append(Diet.Pescetarian.toString());
            else if(diet.equals(Diet.Vegan.toString()))
                sb.append(Diet.Vegan.toString());
            else if(diet.equals(Diet.Vegetarian.toString()))
                sb.append(Diet.Vegetarian.toString());
        }

        //Add intolerances parameters
        if(!intolerances.isEmpty()){
            sb.append("&intolerances=");

            for (String intolerance : intolerances) {
                if(intolerance.equals("dairy"))
                    sb.append("Dairy,");
                if(intolerance.equals("egg"))
                    sb.append("Egg,");
                if(intolerance.equals("gluten"))
                    sb.append("Gluten,");
                if(intolerance.equals("peanut"))
                    sb.append("Peanut,");
                if(intolerance.equals("shellfish"))
                    sb.append("Shellfish,");
            }

            sb = new StringBuilder(sb.substring(0, sb.length() - 1));//remove the last comma ","
        }

        return sb.toString();
    }
}