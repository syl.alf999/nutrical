package com.sylvie.nutrical.utils;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import androidx.appcompat.app.AppCompatDelegate;
import com.sylvie.nutrical.FoodTriviaBroadcast;
import com.sylvie.nutrical.R;
import java.util.Calendar;

/**
 * App starts before any activities
 * We use it to set up some functionnalities like creating notification channels
 * or check preferences to initialize parameters
 */
public class App extends Application {
    public static final String NOTIFICATION_CHANEL_FOOD_TRIVIA = "Food Trivia";

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannels();

        enablingUIMode();

        enablingNotification();

        //Log.i("ALFRED", Constant.FORMAT_DIET_AND_INTOLERANCES_PARAMETERS(getSharedPreferences(Constant.PREFS_FILE_NAME, MODE_PRIVATE)));
    }

    /**
     * Initialize the UI design according the user preference
     */
    public void enablingUIMode(){
        SharedPreferences sp = getSharedPreferences(Constant.PREFS_FILE_NAME, MODE_PRIVATE);
        boolean darkModeEnabled = sp.getBoolean("darkModeEnabled", false);
        int mode = (darkModeEnabled) ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO;
        AppCompatDelegate.setDefaultNightMode(mode);
    }

    /**
     * Initialize wether the notification should be enabled or not
     */
    private void enablingNotification() {
        SharedPreferences sp = getSharedPreferences(Constant.PREFS_FILE_NAME, MODE_PRIVATE);
        boolean notificationsEnabled = sp.getBoolean(Constant.PREFS_notificationsEnabled, true);

        if(notificationsEnabled){
            startAlarm(getApplicationContext());
        }
        else{
            cancelAlarm(getApplicationContext());
        }
    }

    /**
     * Set up the time when the notification should go off
     * Here notifications are triggered on a daily basis à 12:00
     * @param context
     */
    public static void startAlarm(Context context) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 12);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, FoodTriviaBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, 0);

        if(c.before(Calendar.getInstance())){
            c.add(Calendar.DATE, 1);
        }

        //alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
        alarmManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                c.getTimeInMillis(),
                24*60*60*1000,
                pendingIntent);
    }

    /**
     * Cancel the registered notification
     * @param context
     */
    public static void cancelAlarm(Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, FoodTriviaBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, 0);

        alarmManager.cancel(pendingIntent);
    }

    /**
     * Create a channel in the user phone to manage the settings of notifications
     */
    private void createNotificationChannels() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(
                    NOTIFICATION_CHANEL_FOOD_TRIVIA,
                    NOTIFICATION_CHANEL_FOOD_TRIVIA,
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel.setDescription(getString(R.string.food_trivia_notification_description));

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

}