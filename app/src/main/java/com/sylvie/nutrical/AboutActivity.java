package com.sylvie.nutrical;

import android.os.Bundle;

/**
 * About activity
 */
public class AboutActivity extends OptionsMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}