package com.sylvie.nutrical;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.sylvie.nutrical.model.Recipe;

import java.util.List;

/**
 * RecipeAdapter for RecyclerView
 */
public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>{
    private Context context;
    private List<Recipe> recipesList;
    private OnItemClickListener listener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }


    public RecipeAdapter(Context context, List<Recipe> recipesList) {
        this.context = context;
        this.recipesList = recipesList;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recipe_item, parent, false);
        return new RecipeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        Recipe currentRecipe = recipesList.get(position);

        String imageUrl = currentRecipe.getImageUrl();
        String name = currentRecipe.getName();

        holder.tvRecipeName.setText(name);

        Glide
            .with(context)
            .load(imageUrl)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .into(holder.ivRecipePicture);
    }

    @Override
    public int getItemCount() {
        return recipesList.size();
    }


    public class RecipeViewHolder extends RecyclerView.ViewHolder{
        //Elements of the recycler view layout
        ImageView ivRecipePicture;
        TextView tvRecipeName;

        public RecipeViewHolder(@NonNull View itemView) {
            super(itemView);

            //Attach listener to the itemView
            itemView.setOnClickListener(v -> {
                if(listener != null){
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            });

            ivRecipePicture = itemView.findViewById(R.id.ivRecipePicture);
            tvRecipeName = itemView.findViewById(R.id.tvRecipeName);
        }
    }
}