package com.sylvie.nutrical;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatDelegate;
import com.sylvie.nutrical.model.Diet;
import com.sylvie.nutrical.utils.App;
import com.sylvie.nutrical.utils.Constant;
import java.util.HashSet;
import java.util.Set;


public class SettingActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sp = getSharedPreferences(Constant.PREFS_FILE_NAME, MODE_PRIVATE);
        editor = sp.edit();

        //We need to explicitly set the theme of the preference screen
        //Themes are not recognize by nested PreferenceScreen
        boolean darkModeEnabled = sp.getBoolean(Constant.PREFS_darkModeEnabled, false);
        int mode = (darkModeEnabled == true) ? R.style.Theme_AppCompat_DayNight : R.style.Theme_AppCompat_Light;
        setTheme(mode);

        super.onCreate(savedInstanceState);

        //Link the ressource setting file to the Setting Activity
        addPreferencesFromResource(R.xml.settings);
    }

    /**
     * Handle setting whenever the user changes the current settings
     * This method is triggered only if a change is different from the previous state
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key){
            case "diet":
                updateDiet(sharedPreferences);
                break;
            case "intolerance":
                updateIntolerances(sharedPreferences);
                break;
            case "notification":
                boolean notificationEnabled = sharedPreferences.getBoolean("notification", true);
                updateNotifications(notificationEnabled);
                break;
            case "dark_mode":
                boolean darkModeEnabled = sharedPreferences.getBoolean("dark_mode", false);
                updateDarkMode(darkModeEnabled);
                break;
        }

    }

    /**
     * Change the current diet (pescetarian, vegetarian, vegan) of the user
     * @param sp
     */
    private void updateDiet(SharedPreferences sp){
        String diet_value = sp.getString(Constant.PREFS_diet, "no_particular_diet");
        //Log.i("ALFRED", diet_value);

        switch (diet_value){
            case "pescetarian":
                editor.putString(Constant.PREFS_diet, Diet.Pescetarian.toString());
                break;
            case "vegetarian":
                editor.putString(Constant.PREFS_diet, Diet.Vegetarian.toString());
                break;
            case "vegan":
                editor.putString(Constant.PREFS_diet, Diet.Vegan.toString());
                break;
            default:
                editor.putString(Constant.PREFS_diet, Diet.NoDiet.toString());
                break;
        }

        editor.apply();
    }

    /**
     * Change the current intolerances (milk, dairies, peanuts, eggs) of the user
     * We need to set back the intolerances to none as the list of intolerances received in
     * SharedPreferences sp returns only the checkboxes set to true.
     * @param sp
     */
    private void updateIntolerances(SharedPreferences sp){
        Set<String> intolerances = sp.getStringSet(Constant.PREFS_intolerances, new HashSet<String>());
        editor.putStringSet(Constant.PREFS_intolerances, intolerances);
        editor.apply();
    }

    /**
     * Change the UI design of the app between light and dark mode
     * @param darkModeEnabled
     */
    private void updateDarkMode(boolean darkModeEnabled){
        Log.i("ALFRED","darkmode : " + darkModeEnabled);

        int mode = (darkModeEnabled) ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO;
        AppCompatDelegate.setDefaultNightMode(mode);

        editor.putBoolean(Constant.PREFS_darkModeEnabled, darkModeEnabled);
        editor.apply();

        Toast.makeText(this, "Mode changed !", Toast.LENGTH_SHORT).show();
    }

    /**
     * Enable or disable notifications according the user preference
     * It will cancel the triggering of the alarm if set to off
     * @param notificationEnabled
     */
    private void updateNotifications(boolean notificationEnabled){
        Log.i("ALFRED","notifications : " + notificationEnabled);
        editor.putBoolean(Constant.PREFS_notificationsEnabled, notificationEnabled);
        editor.apply();

        if(notificationEnabled){
            App.startAlarm(this);
        }
        else{
            App.cancelAlarm(this);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}