package com.sylvie.nutrical.model;

public enum Diet {
    Vegan, Vegetarian, Pescetarian, NoDiet
}
