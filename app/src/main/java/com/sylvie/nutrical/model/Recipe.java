package com.sylvie.nutrical.model;

import java.util.HashMap;

public class Recipe {

    //DEFINING THE KEY VALUES OF THE TWO HASHMAPS
    public static String KEY_CALORIES = "calories";
    public static String KEY_FAT = "fat";
    public static String KEY_CARBS = "carbs";
    public static String KEY_PROTEIN = "protein";

    private String name;
    private String imageUrl;
    private String instructions;
    private HashMap<String, String> nutritionsValues;
    private HashMap<String, String> nutritionsPercentOfDailyNeeds;

    public Recipe(String name, String imageUrl, String instructions,
                  HashMap<String,String> nutritionsValues,
                  HashMap<String, String> nutritionsPercentOfDailyNeeds){
        this.name = name;
        this.imageUrl = imageUrl;
        this.instructions = instructions;
        this.nutritionsValues = nutritionsValues;
        this.nutritionsPercentOfDailyNeeds = nutritionsPercentOfDailyNeeds;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getInstructions(){ return instructions; }

    public HashMap<String, String> getNutritionsValues() {
        return nutritionsValues;
    }

    public HashMap<String, String> getNutritionsPercentOfDailyNeeds() {
        return nutritionsPercentOfDailyNeeds;
    }

}